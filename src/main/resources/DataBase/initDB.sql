CREATE TABLE IF NOT EXISTS employees (
    id INT PRIMARY KEY ,
    surname VARCHAR(100) NOT NULL ,
    name VARCHAR(50) NOT NULL ,
    patronymic VARCHAR(100)
);