package com.demoDBConnection.DB.services;

import com.demoDBConnection.DB.models.Employee;

import java.util.List;

public interface EmployeeService {

    Employee getById(int id);

    List<Employee> getAll();

    void save(Employee employee);

    void delete(int id);
}
