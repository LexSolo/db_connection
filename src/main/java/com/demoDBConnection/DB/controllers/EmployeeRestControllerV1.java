package com.demoDBConnection.DB.controllers;

import com.demoDBConnection.DB.models.Employee;
import com.demoDBConnection.DB.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/employees/")
public class EmployeeRestControllerV1 {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeRestControllerV1(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployee(@PathVariable("id") Integer employeeId) {
        if (employeeId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Employee employee = this.employeeService.getById(employeeId);

        if (employee == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> employees = this.employeeService.getAll();

        if (employees.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> saveEmployee(@RequestBody @Valid Employee employee) {
        HttpHeaders httpHeaders = new HttpHeaders();

        if (employee == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        this.employeeService.save(employee);

        return new ResponseEntity<>(employee, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> updateEmployee(@RequestBody @Valid Employee employee, UriComponentsBuilder builder) {
        HttpHeaders httpHeaders = new HttpHeaders();

        if (employee == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        this.employeeService.save(employee);

        return new ResponseEntity<>(employee, httpHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") Integer employeeId) {
        Employee employee = this.employeeService.getById(employeeId);

        if (employee == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        this.employeeService.delete(employeeId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
